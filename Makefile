NAME=nats-server
NSPC=hacknizer
TNSP=hacknizer



.PHONY: nats del status full delfull statusfull

nats:
	helm install -n ${NAME} --namespace ${NSPC} ./

del:
	helm del --purge ${NAME}

status:
	helm status ${NAME}




################
##### TEST #####
################

full:
	make nats
	kubectl apply -f templates/_producer.yaml
	kubectl apply -f templates/_consumer.yaml

delfull:
	kubectl delete deployment --namespace ${TNSP} nats-producer
	kubectl delete deployment --namespace ${TNSP} nats-consumer
	kubectl delete service --namespace ${TNSP} nats-producer
	kubectl delete service --namespace ${TNSP} nats-consumer
	helm del --purge ${NAME}

statusfull:
	kubectl get all --namespace ${TNSP}
